import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";
// import { Button, Image, Modal, Icon } from 'semantic-ui-react'
// import 'semantic-ui-css/semantic.min.css'
import "../App.css";

import Header from "../components/Header";
import Cover from "../components/Cover";
import Counter from "../components/Counter";
import Couple from "../components/Couple";
import Story from "../components/Story";
import Quote from "../components/Quote";
import Footer from "../components/Footer";
import Event from "../components/Event";
import Guest from "../components/Guest";
import Gallery from "../components/Gallery";
import Modal from "../components/Modal";

// import globalUrl from "../actions/config";

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			order: null,
			invite: null,
			modal: true,
			music: false,
		};
	}

	componentDidMount = () => {
		const url_string = document.URL;
		const url = new URL(url_string);
		const hostname = url.hostname;
		const hostnames = hostname.split(".");
		const param = url.searchParams.get("invite");

		axios
			.get(
				`${process.env.REACT_APP_API_URL}/findOrder?domain=${hostnames[0]}?invite=${param}`
			)
			.then((response) => {
				this.setState({ order: response.data.order, invite: response.data.invite });
			})
			.catch((error) => {
				throw error;
			});
	};

	onAction = (flag) => {
		if (flag === "calendar") {
			var startDate = moment(this.state.order.events[0].startDate).format(
				"YYYYMMDD"
			);
			var startTime = moment(this.state.order.events[0].startDate).format(
				"HHmmss"
			);
			var endDate = moment(this.state.order.events[0].endDate).format("YYYYMMDD");
			var endTime = moment(this.state.order.events[0].endDate).format("HHmmss");
			var location = this.state.order.events[0].address;

			// var event = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T110000/${endDate}T200000&ctz=Asia/Jakarta;`;
			var url = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T${startTime}/${endDate}T${endTime}&ctz=Asia/Jakarta&details=Kami akan sangat gembira ketika kita bisa bertemu di acara kami.<br><br>salam hangat&location=${location}&pli=1&amp;uid=1521339627addtocalendar&sf=true&amp;output=xml`;
			var win = window.open(url, "_blank");
			win.focus();
		}
	};

	onSubmit = (payload) => {
		axios
			.post(
				`${process.env.REACT_APP_API_URL}/insertOneWish/${this.state.order.id}`,
				payload
			)
			.then(({ data }) => {
				this.setState({ order: data });
				toast.success("Data saved", {
					position: toast.POSITION.TOP_RIGHT,
				});
			})
			.catch((error) => {
				throw error;
			});
	};

	onTogglePlay(param) {
		var x = document.getElementById("myAudio");
		if (param === "play") {
			x.play();
		} else {
			x.pause();
		}
		this.setState({ music: !this.state.music });
	}

	render() {
		return (
			<>
				<div className="application">
					<Helmet>
						<meta charSet="utf-8" />
						<title>{this.state.order && this.state.order.name}</title>
					</Helmet>
				</div>
				<Header />
				<Cover
					couples={this.state.order ? this.state.order.detail.couples : null}
					images={this.state.order ? this.state.order.detail.images : null}
					events={this.state.order ? this.state.order.detail.events : null}
				/>
				<Couple
					couples={this.state.order ? this.state.order.detail.couples : null}
				/>
				<Counter
					events={this.state.order ? this.state.order.detail.events : null}
					images={this.state.order ? this.state.order.detail.images : null}
				/>
				<Story
					stories={this.state.order ? this.state.order.detail.stories : null}
				/>
				<Quote
					quotes={this.state.order ? this.state.order.detail.quotes : null}
					images={this.state.order ? this.state.order.detail.images : null}
				/>
				<Event
					events={this.state.order ? this.state.order.detail.events : null}
					onAction={(flag) => this.onAction(flag)}
				/>
				<Guest
					wishes={this.state.order ? this.state.order.detail.wishes : null}
					onSubmit={(e) => this.onSubmit(e)}
				/>
				<Gallery
					images={this.state.order ? this.state.order.detail.images : null}
				/>
				<Footer />
				<ToastContainer />
				<audio
					loop=""
					preload="auto"
					id="myAudio"
					src={this.state.order && this.state.order.detail.songs[0].url}
				/>
				{!this.state.music ? (
					<a href="#home" class="music" onClick={() => this.onTogglePlay("play")}>
						<i class="lni-music"></i>
					</a>
				) : (
					<a href="#home" class="music" onClick={() => this.onTogglePlay("pause")}>
						<i class="lni-volume-high"></i>
					</a>
				)}
				{this.state.invite ? (
					<Modal
						visible={this.state.modal}
						invite={this.state.invite}
						onClose={() => {
							this.setState({ modal: false });
						}}
					/>
				) : null}
			</>
		);
	}
}

export default Home;
