import React from "react";

const Couple = ({ couples }) => {
	return (
		<div id="qbootstrap-groom-bride" data-section="groom-bride">
			<div className="container">
				<div className="row animate-box">
					<div className="col-md-8 col-md-offset-2">
						<div className="col-md-12 text-center section-heading svg-sm-2 colored">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/002-wedding-couple.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2>About Us</h2>
							{/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam accusamus, sequi, minima repellendus explicabo magni aperiam, ducimus perferendis ad quidem suscipit omnis unde veritatis pariatur. Commodi, nisi. Iusto, accusantium a.</p> */}
						</div>
					</div>
				</div>
				<div className="row">
					{couples &&
						couples.map((data, i) => {
							return (
								<div className="col-md-6" key={i}>
									<div className="couple groom text-center">
										<img
											src={data.images[0].url}
											className="img-responsive"
											alt="Free HTML5 Bootstrap Template by QBootstrap.com"
										/>
										<div className="desc">
											<h2>{data.fullName}</h2>
											<p>{data.description}</p>
											<ul className="social social-circle">
												{/* <li><a href="#"><i className="icon-twitter"></i></a></li>
                        <li><a href="#"><i className="icon-facebook"></i></a></li>
                        <li><a href="#"><i className="icon-instagram"></i></a></li> */}
											</ul>
										</div>
									</div>
								</div>
							);
						})}
				</div>
			</div>
		</div>
	);
};

export default Couple;
