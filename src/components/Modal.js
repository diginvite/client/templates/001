import React from "react";
import Modal from "react-awesome-modal";

const Modal2 = (props) => {
	return (
		<section className="text-center">
			<Modal
				visible={props.visible}
				width="400"
				height="300"
				effect="fadeInUp"
				onClickAway={() => props.onClose()}
			>
				<div>
					<br />
					<h1>Hallo {props.invite && props.invite.name}</h1>
					<p>We invited you to celebrate our wedding</p>
					<h1>
						<i class="lni-envelope size-lg"></i>
					</h1>
					<a href="#home" onClick={() => props.onClose()}>
						Open
					</a>
				</div>
			</Modal>
		</section>
	);
};

export default Modal2;
