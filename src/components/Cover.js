import React, { useState, useEffect } from "react";

const Cover = ({ images, couples, events }) => {
	const [image, setImage] = useState(null);
	useEffect(() => {
		if (images) {
			const _images = images.filter((image, index) => image.isCover === true);
			if (_images.length) {
				setImage(_images[0]);
			}
		}
	}, [images]);
	return (
		<>
			<div className="qbootstrap-hero" data-section="home">
				<div className="qbootstrap-overlay"></div>
				<div
					className="qbootstrap-cover text-center"
					data-stellar-background-ratio="0.5"
					style={{
						backgroundImage: `url(${image && image.url})`,
					}}
				>
					<div className="display-t">
						<div className="display-tc">
							<div className="container">
								<div className="col-md-10 col-md-offset-1">
									<div className="animate-box svg-sm colored">
										<img
											src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/004-nature.svg"
											className="svg"
											alt="Free HTML5 Bootstrap Template by QBootstrap.com"
										/>
										<h1 className="holder">
											<span>The Wedding of</span>
										</h1>
										<h2>
											{couples && couples[0].nickName} &amp;{" "}
											{couples && couples[1].nickName}
										</h2>
										<p>{events && events[0].startDate}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Cover;
