import React from "react";

const Header = (props) => {
	return (
		<header role="banner" id="qbootstrap-header">
			<div className="container">
				{/* <div className="row"> */}
				<nav className="navbar navbar-default">
					<div className="navbar-header">
						<a
							href="#home"
							className="js-qbootstrap-nav-toggle qbootstrap-nav-toggle"
							data-toggle="collapse"
							data-target="#navbar"
							aria-expanded="false"
							aria-controls="navbar"
						>
							<i></i>
						</a>
						<a className="navbar-brand" href="#home">
							Wedding
						</a>
					</div>
					<div id="navbar" className="navbar-collapse collapse">
						<ul className="nav navbar-nav navbar-right">
							{/* <li className="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
                <li><a href="#" data-nav-section="groom-bride"><span>Groom &amp; Bride</span></a></li>
                <li><a href="#" data-nav-section="story"><span>Love Story</span></a></li>
                <li><a href="#" data-nav-section="greetings"><span>Greetings</span></a></li>
                <li><a href="#" data-nav-section="people"><span>People</span></a></li>
                <li><a href="#" data-nav-section="when-where"><span>When &amp; Where</span></a></li>
                <li><a href="#" data-nav-section="rsvp"><span>RSVP</span></a></li>
                <li><a href="#" data-nav-section="gallery"><span>Gallery</span></a></li>
                <li><a href="#" data-nav-section="blog"><span>Blog</span></a></li> */}
						</ul>
					</div>
				</nav>
				{/* </div> */}
			</div>
		</header>
	);
};

export default Header;
