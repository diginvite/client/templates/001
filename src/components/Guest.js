import React, { useEffect, useState } from "react";
// import Swiper from "react-id-swiper";
// import 'react-id-swiper/lib/styles/css/swiper.css';
// import 'react-id-swiper/lib/styles/scss/swiper.scss';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { toast } from "react-toastify";

// const params = {
// 	spaceBetween: 30,
// 	centeredSlides: true,
// 	autoplay: {
// 		delay: 2500,
// 		disableOnInteraction: false,
// 	},
// 	navigation: {
// 		nextEl: ".swiper-button-next",
// 		prevEl: ".swiper-button-prev",
// 	},
// };

const settings = {
	dots: true,
	infinite: true,
	// speed: 500,
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	speed: 3000,
	autoplaySpeed: 2000,
};

const Guest = ({ wishes, onSubmit }) => {
	const [wishesCompailed, setWishesCompailed] = useState(null);
	const [guestInput, setGuestInput] = useState({
		name: "",
		form: "",
		person: 1,
		attend: 1,
		wish: "",
	});

	useEffect(() => {
		let _wishes = [];
		if (wishes) {
			// eslint-disable-next-line
			wishes.map((data, i) => {
				_wishes.push(
					<div key={i}>
						<h1 style={{ color: "white" }}>{data.wish}</h1>
						<h3 style={{ color: "white" }}>{data.name}</h3>
					</div>
				);
			});
		}
		setWishesCompailed(_wishes);
	}, [wishes]);

	const handleChange = (e) => {
		const { name, value } = e.target;
		setGuestInput({ ...guestInput, [name]: value });
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (
			guestInput.name === "" ||
			guestInput.from === "" ||
			guestInput.wish === ""
		) {
			toast.error("Name, from and wish cannt be null", {
				position: toast.POSITION.TOP_RIGHT,
			});
		} else {
			onSubmit(guestInput);
			setGuestInput({ name: "", form: "", person: 1, attend: 1, wish: "" });
		}
	};

	return (
		<div
			id="qbootstrap-started"
			className=""
			data-section="rsvp"
			data-stellar-background-ratio="0.5"
			// style={{backgroundImage: `url(${props.data && props.data.covers[0].path})`}}
		>
			<div className="overlay"></div>
			<div className="container">
				<div className="row animate-box">
					<div className="col-md-8 col-md-offset-2">
						<div className="col-md-12 text-center section-heading svg-sm colored">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/005-two.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2>You Are Invited</h2>
							<div className="row">
								{/* <div className="col-md-10 col-md-offset-1 subtext">
                  <h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
                </div> */}
							</div>
						</div>
					</div>
				</div>
				<div className="text-center" style={{ color: "white" }}>
					{/* <Swiper {...params}>
              {wishes}
            </Swiper> */}
					<Slider {...settings}>{wishesCompailed}</Slider>
				</div>
				<br />
				<br />
				<br />
				<div className="row animate-box">
					<div className="col-md-12">
						<form className="form-inline" onSubmit={handleSubmit}>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label htmlFor="name" className="sr-only">
										Name
									</label>
									<input
										type="text"
										className="form-control"
										id="name"
										placeholder="Name"
										name="name"
										value={guestInput.name}
										onChange={(e) => handleChange(e)}
									/>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label htmlFor="from" className="sr-only">
										From
									</label>
									<input
										type="text"
										className="form-control"
										id="company"
										placeholder="From"
										name="from"
										value={guestInput.from}
										onChange={(e) => handleChange(e)}
									/>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label htmlFor="attend" className="sr-only">
										Attend
									</label>
									<select
										className="form-control"
										name="attend"
										value={guestInput.attend}
										onChange={(e) => handleChange(e)}
									>
										<option value="1">Yes</option>
										<option value="2">Tentative</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label htmlFor="person" className="sr-only">
										Person
									</label>
									<select
										className="form-control"
										name="person"
										value={guestInput.person}
										onChange={(e) => handleChange(e)}
									>
										<option value="1">1 Person</option>
										<option value="2">2 Persons</option>
									</select>
								</div>
							</div>
							<div className="col-md-12 col-sm-12">
								<div className="form-group">
									<label htmlFor="wish" className="sr-only">
										Wish
									</label>
									<textarea
										className="form-control"
										placeholder="Wish"
										rows="5"
										name="wish"
										value={guestInput.wish}
										onChange={(e) => handleChange(e)}
									></textarea>
								</div>
							</div>
							<div className="col-md-12 col-sm-12">
								<button type="submit" className="btn btn-default btn-block">
									Send
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Guest;
