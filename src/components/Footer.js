import React from 'react';

const Footer = () => {
  return(
    <>
      <footer id="footer" role="contentinfo">
        <div class="container">
          <div class="row row-bottom-padded-sm">
            <div class="col-md-12">
              <p class="copyright text-center">&copy; 2019 <a href="http://diginvite.com" target="blank">Diginvite</a>. All Rights Reserved.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <ul class="social social-circle">
                <li><a href="http://twitter.com/diginvite_" target="blank"><i class="icon-twitter"></i></a></li>
                {/* <li><a href="#" target="blank"><i class="icon-facebook"></i></a></li> */}
                {/* <li><a href="#" target="blank"><i class="icon-youtube"></i></a></li> */}
                <li><a href="http://instagram.com/diginvite_" target="blank"><i class="icon-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer;