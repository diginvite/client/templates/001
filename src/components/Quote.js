import React, { useState, useEffect } from "react";

const Quote = ({ quotes, images }) => {
	const [image, setImage] = useState(null);
	useEffect(() => {
		if (images) {
			const _images = images.filter((image, index) => image.isCover === true);
			if (_images.length) {
				setImage(_images[0]);
			}
		}
	}, [images]);

	return (
		<>
			<div
				id="qbootstrap-testimonials"
				className="qbootstrap-greetings"
				data-section="greetings"
				// data-stellar-background-ratio="0.5"
				style={{
					backgroundImage: `url(${image ? image.url : null})`,
					height: "100vh",
				}}
			>
				<div className="overlay"></div>
				<div className="container">
					<div className="row animate-box">
						<div className="col-md-12 section-heading text-center svg-sm colored">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/006-flower-bell-outline-design-variant-with-vines-and-leaves.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2 className="">Sweet Messages</h2>
						</div>
					</div>
					<div className="row">
						{quotes &&
							quotes.map((data, i) => {
								return (
									<div className="col-md-4" key={i}>
										<div className="box-testimony ">
											<blockquote>
												<span className="quote">
													<span>
														<i className="icon-quote-left"></i>
													</span>
												</span>
												<p>&ldquo;{data.description}&rdquo;</p>
											</blockquote>
											{/* <p className="author">John Doe</p> */}
										</div>
									</div>
								);
							})}
					</div>
				</div>
			</div>
		</>
	);
};

export default Quote;
