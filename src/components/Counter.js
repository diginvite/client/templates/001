import React, { useState, useEffect } from "react";
import Countdown from "react-countdown-now";
import Moment from "react-moment";

const renderer = ({ days, hours, minutes, seconds }) => {
	return (
		<p className="countdown">
			<span>
				{days}
				<small>Days</small>
			</span>
			<span>
				{hours}
				<small>Hours</small>
			</span>
			<span>
				{minutes}
				<small>Minutes</small>
			</span>
			<span>
				{seconds}
				<small>Seconds</small>
			</span>
		</p>
	);
};

const Counter = ({ events, images }) => {
	const [image, setImage] = useState(null);

	useEffect(() => {
		if (images) {
			const _images = images.filter((image, index) => image.isCover === true);
			if (_images.length) {
				setImage(_images[0]);
			}
		}
	}, [images]);

	return (
		<div
			id="qbootstrap-countdown"
			// data-stellar-background-ratio="0.5"
			style={{
				backgroundImage: `url(${image && image.url})`,
			}}
			data-section="wedding-day"
		>
			<div className="overlay"></div>
			<div className="display-over">
				<div className="container">
					<div className="row animate-box">
						<div className="col-md-12 section-heading text-center svg-sm colored">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/006-flower-bell-outline-design-variant-with-vines-and-leaves.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2 className="">The Wedding Day</h2>
							<span className="datewed">
								<Moment format="dddd, MMM. DD, YYYY">
									{events && events[0].startDate}
								</Moment>
							</span>
						</div>
					</div>
					<div className="row animate-box">
						<div className="col-md-8 col-md-offset-2 text-center">
							<Countdown date={events && events[0].startDate} renderer={renderer} />
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Counter;
