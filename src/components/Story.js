import React from "react";
import Moment from "react-moment";

const Story = ({ stories }) => {
	return (
		<div id="qbootstrap-story" data-section="story">
			<div className="container">
				<div className="row animate-box">
					<div className="col-md-8 col-md-offset-2">
						<div className="col-md-12 text-center section-heading svg-sm-2">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/003-luxury.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2>Our Love Story</h2>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-12">
						<ul className="timeline animate-box">
							{stories &&
								stories.map((data, i) => {
									let className = "";
									if (i % 2 !== 0) {
										className = "timeline-inverted";
									}
									return (
										<li className={className} key={i}>
											{/* <div className="timeline-badge" style="background-image:url(images/couple-1.jpg);"></div> */}
											<div className="timeline-panel">
												<div className="overlay"></div>
												<div className="timeline-heading">
													<h3 className="timeline-title">{data.title}</h3>
													<span className="date">
														<Moment format="MMMM DD, YYYY">{data.date}</Moment>
													</span>
												</div>
												<div className="timeline-body">
													<p>{data.description}</p>
												</div>
											</div>
										</li>
									);
								})}
						</ul>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Story;
