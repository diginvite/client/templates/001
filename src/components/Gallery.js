import React, { useState, useEffect } from "react";
import Gallery from "react-grid-gallery";

const Gallery2 = ({ images }) => {
	const [galleries, setGalleries] = useState([]);

	useEffect(() => {
		if (images) {
			let _galleries = [];
			// eslint-disable-next-line
			images.map((image) => {
				if (image.isGallery === true) {
					const _image = {
						src: image.url,
						thumbnail: image.url,
						// thumbnailWidth: 320,
						// thumbnailHeight: 174,
						// isSelected: true,
						caption: image.caption,
					};
					_galleries.push(_image);
				}
			});
			setGalleries(_galleries);
		}
	}, [images]);

	return (
		<>
			<div id="qbootstrap-press" data-section="blog">
				<div className="container">
					<div className="row animate-box">
						<div className="col-md-8 col-md-offset-2">
							<div className="col-md-12 text-center section-heading svg-sm colored">
								<img
									src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/005-two.svg"
									className="svg"
									alt="Free HTML5 Bootstrap Template by QBootstrap.com"
								/>
								<h2>Our Selfie Photos</h2>
								<div className="row">
									<div className="col-md-10 col-md-offset-1 subtext">
										<h3>
											Far far away, behind the word mountains, far from the countries
											Vokalia and Consonantia, there live the blind texts.
										</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="container-fluid">
					<div className="container">
						<div className="row">
							<Gallery images={galleries} />
							{/* {
              props.data && props.data.galleries.map((data, i) => {
                return(
                  <div class="col-md-3 col-sm-6" key={i}>
                    <div class="gallery">
                      <a class="gallery-img image-popup image-popup" href={data.path}>
                        <img src={data.path} class="img-responsive" alt="Free HTML5 Bootstrap Template by QBootstrap.com"/>
                      </a>
                    </div>
                  </div>
                )
              })
            } */}
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Gallery2;
