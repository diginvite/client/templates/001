import React from "react";
import Moment from "react-moment";

const Event = ({ events, onAction, google }) => {
	return (
		<div id="qbootstrap-when-where" data-section="when-where">
			<div className="container">
				<div className="row">
					<div className="col-md-8 col-md-offset-2">
						<div className="col-md-12 text-center section-heading svg-sm colored">
							<img
								src="https://demos.onepagelove.com/html/qbwedding/images/flaticon/svg/005-two.svg"
								className="svg"
								alt="Free HTML5 Bootstrap Template by QBootstrap.com"
							/>
							<h2>Ceremony &amp; Party</h2>
							<div className="row">
								<div className="col-md-10 col-md-offset-1 subtext">
									<h3>
										Far far away, behind the word mountains, far from the countries
										Vokalia and Consonantia, there live the blind texts.
									</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row row-bottom-padded-md">
					{events &&
						events.map((data, i) => {
							return (
								<div className="col-md-6 text-center" key={i}>
									<div className="wedding-events">
										{/* <div className="ceremony-bg" style="background-image: url(images/wed-ceremony.jpg);"></div> */}
										<div className="desc">
											<h3>{data.name}</h3>
											<div className="row">
												<div className="col-md-2 col-md-push-5">
													<div className="icon-tip">
														<span className="icon">
															<i className="icon-heart-o"></i>
														</span>
													</div>
												</div>
												<div className="col-md-5 col-md-pull-1">
													<div className="date">
														<i className="icon-calendar"></i>
														<span>
															<Moment format="dddd">{data.startDate}</Moment>
														</span>
														<span>
															<Moment format="DD MMM. YYYY">{data.startDate}</Moment>
														</span>
													</div>
												</div>
												<div className="col-md-5 col-md-pull-1">
													<div className="date">
														<i className="icon-clock2"></i>
														<span>
															<Moment format="HH:mm A">{data.startDate}</Moment>
														</span>
														<span>
															<Moment format="HH:mm A">{data.endDate}</Moment>
														</span>
													</div>
												</div>
											</div>
											<p>
												{data.address}
												<br />
												<small>
													<i>"{data.description}"</i>
												</small>
											</p>
											<p>
												<a
													className="btn btn-primary btn-sm"
													href={`http://maps.google.com/maps?z=12&t=m&q=loc:${data.lat}+${data.lng}`}
													target="blank"
												>
													Maps
												</a>
												&nbsp;
												<a
													href="#home"
													className="btn btn-primary btn-sm"
													onClick={() => onAction("calendar")}
												>
													Add to calendar
												</a>
											</p>
										</div>
									</div>
								</div>
							);
						})}
					<div className="row">
						<div className="col-md-6">
							<div id="map" className="qbootstrap-map"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Event;
